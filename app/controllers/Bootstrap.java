package controllers;
/*
 * Copyright © Sartini IT Solutions, 2010
 */


import models.Page;
import play.jobs.*;
import play.test.*;

/**
 *
 * @author Pierangelo Sartini
 */
@OnApplicationStart
public class Bootstrap extends Job {

    public void doJob() {
        // Check if the database is empty
        if(Page.count() == 0) {
            Fixtures.loadModels("initial-data.yml");
        }
    }

}
