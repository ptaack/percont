/*
 * Copyright © Sartini IT Solutions, 2010.
 */
package tags;

import groovy.lang.Closure;
import models.GalleryFile;
import models.GalleryGroup;
import play.exceptions.TemplateNotFoundException;
import play.templates.FastTags;
import play.templates.GroovyTemplate;
import play.templates.Template;
import play.templates.TemplateLoader;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import play.templates.TagContext;
import static controllers.Const.PAGE_CONTEXT;

@FastTags.Namespace("")
public class GalleryTags extends FastTags {
	public static void _gallery(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
		final String _arg = args.get("arg").toString();
		List<GalleryFile> gFiles = GalleryFile.find("byTitle", _arg).fetch();
		for (GalleryFile galleryFile : gFiles) {
			body.setProperty("_", galleryFile);
			body.call();
		}
	}
}
