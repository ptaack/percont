package models;

import java.io.File;
import java.io.IOException;

import javax.persistence.*;

import org.apache.commons.io.FilenameUtils;

import play.Play;
import play.db.jpa.Blob;
import play.db.jpa.JPABase;
import play.db.jpa.Model;
import play.libs.Images;

@Entity
public class GalleryFile extends Model {
	@OneToOne
	public GalleryGroup group;
	public String title;
	public String description;
	public Blob file;
	
	/**
	 * !Нужно Добавить Crop 260x180
	 * !Нужно Реализовать функцию для рендеринда, что бы избавиться от staticDir:photos
	 * @return 
	 * @throws IOException
	 */
	public String thumbnail() throws IOException {
		String thumbnailPath = "";
		if (!this.file.exists()) {
			thumbnailPath = "http://placehold.it/260x180";
			return thumbnailPath;			
		}
		String extension = this.file.type().split("/")[1];
		File original = this.file.getFile();
		File thumbnail = new File(original.getParent(), FilenameUtils.getBaseName(original.getName())
				.concat("." + "thumbnail")
				.concat("." + extension)
				);
		if (!thumbnail.exists()) {
			Images.resize(this.file.getFile(), thumbnail, 260, -1);
		}
		thumbnailPath = "/" + Play.configuration.getProperty("attachments.path") + "/" + thumbnail.getName();
		return thumbnailPath;
	}
	
	public String toString() {
		return this.title;
	}
}