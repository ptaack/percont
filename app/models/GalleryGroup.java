package models;

import javax.persistence.*;

import play.db.jpa.Model;

import java.util.ArrayList;
import java.util.List;

@Entity
public class GalleryGroup extends Model {
	public String title;
	
	public String toString() {
		return this.title;
	}
}