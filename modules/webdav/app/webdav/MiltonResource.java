package webdav;

import com.bradmcevoy.http.*;
import com.bradmcevoy.http.exceptions.BadRequestException;
import com.bradmcevoy.http.exceptions.ConflictException;
import com.bradmcevoy.http.exceptions.NotAuthorizedException;
import models.Page;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author Piero Sartini
 */
public class MiltonResource implements Resource, GetableResource, PropFindableResource, FolderResource {

	private Page page;

	public MiltonResource(Page p) {
		this.page = p;
	}

	@Override
	public String getUniqueId() {
		return String.valueOf(page.id);
	}

	@Override
	public String getName() {
		return page.path;
	}

	@Override
	public Object authenticate(String s, String s1) {
		return null;
	}

	@Override
	public boolean authorise(Request request, Request.Method method, Auth auth) {
		return true;
	}

	@Override
	public String getRealm() {
		return null;
	}

	@Override
	public Date getModifiedDate() {
		return page.lastChanged;
	}

	@Override
	public String checkRedirect(Request request) {
		return null;
	}

	@Override
	public void sendContent(OutputStream outputStream, Range range, Map<String, String> stringStringMap, String s) throws IOException, NotAuthorizedException, BadRequestException {
		String content = page.getInheritedPart("body").content;
		outputStream.write(content.getBytes());
	}

	@Override
	public Long getMaxAgeSeconds(Auth auth) {
		return null;
	}

	@Override
	public String getContentType(String s) {
		return null;
	}

	@Override
	public Long getContentLength() {
		return new Integer(page.getInheritedPart("body").content.length()).longValue();
	}

	@Override
	public Date getCreateDate() {
		return page.created;
	}

	@Override
	public void copyTo(CollectionResource collectionResource, String s) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void delete() throws NotAuthorizedException, ConflictException, BadRequestException {
		page.delete();
	}

	@Override
	public CollectionResource createCollection(String s) throws NotAuthorizedException, ConflictException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void moveTo(CollectionResource collectionResource, String s) throws ConflictException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Resource createNew(String s, InputStream inputStream, Long aLong, String s1) throws IOException, ConflictException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Resource child(String s) {
		return new MiltonResource(Page.findChild(page, s));
	}

	@Override
	public List<? extends Resource> getChildren() {
		List<MiltonResource> resultList = new ArrayList<MiltonResource>();
		for (Page child : page.childs) {
			resultList.add(new MiltonResource(child));
		}
		return resultList;
	}
}
