package webdav;

import com.bradmcevoy.http.Resource;
import com.bradmcevoy.http.ResourceFactory;
import models.Page;

/**
 * @author Piero Sartini
 */
public class MiltonResourceFactory implements ResourceFactory {
	@Override
	public Resource getResource(String host, String path) {
		path = path.replaceAll("/dav/","/");
		Page p = Page.findPageForRequest(host, path);
		if (p != null) {
		return new MiltonResource(p);
		} else {
			return null;
		}
	}
}
