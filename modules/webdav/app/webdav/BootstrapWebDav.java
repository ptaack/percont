package webdav;

import com.bradmcevoy.http.ResourceFactory;
import milton.HttpManagerFactory;
import play.jobs.Job;
import play.jobs.OnApplicationStart;

/**
 * @author Piero Sartini
 */
@OnApplicationStart
public class BootstrapWebDav extends Job {
	public void doJob() {
		HttpManagerFactory.setResourceFactory(new MiltonResourceFactory());
	}
}
