package milton;

import com.bradmcevoy.http.HttpManager;
import com.bradmcevoy.http.ResourceFactory;
import play.Logger;

/**
 * @author Piero Sartini
 */
public class HttpManagerFactory {
	private static HttpManager httpManager;
	private static ResourceFactory resourceFactory;

	public static void setResourceFactory(ResourceFactory resourceFactory) {
		HttpManagerFactory.resourceFactory = resourceFactory;
	}
	
	public static HttpManager getHttpManager() {
		if (httpManager == null) {
			if (resourceFactory == null) {
				Logger.error("Need to call setResourceFactory() before obtaining the HttpManager.");
				throw new RuntimeException("Configuration Error.");
			}
			httpManager = new HttpManager(resourceFactory);
		}
		System.out.println("Providing HttpManager: "+httpManager);
		return httpManager;
	}
}
