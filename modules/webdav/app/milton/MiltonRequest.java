package milton;

import com.bradmcevoy.http.*;
import com.bradmcevoy.io.StreamUtils;
import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.RequestContext;
import org.apache.commons.io.output.ByteArrayOutputStream;
import play.mvc.Http;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Piero Sartini
 */
public class MiltonRequest extends AbstractRequest {

	private Http.Request request;
	private Auth auth;
	
	public MiltonRequest(Http.Request request) {
		this.request = request;
	}

	@Override
	public Map<String, String> getHeaders() {
		Map<String, String> resultMap = new HashMap<String, String>();
		for (Map.Entry<String, Http.Header> entry : request.headers.entrySet()) {
			resultMap.put(entry.getValue().name, entry.getValue().value());
		}
		return resultMap;
	}

	@Override
	public String getFromAddress() {
		return request.remoteAddress;
	}

	@Override
	public String getRequestHeader(Header header) {
		Http.Header h = request.headers.get(header.code.toLowerCase());
		if (h == null)
			return null;
		else {
			return h.value();
		}
	}

	@Override
	public Method getMethod() {
		return Method.valueOf(request.method);
	}

	@Override
	public Auth getAuthorization() {
            if( auth != null ) return auth;
            String enc = getRequestHeader( Header.AUTHORIZATION );
            if( enc == null ) return null;
            if( enc.length() == 0 ) return null;
            auth = new Auth( enc );
            return auth;
	}

	@Override
	public String getAbsoluteUrl() {
		return request.getBase()+request.url;
	}

	@Override
	public InputStream getInputStream() throws IOException {
		return request.body;
	}

	@Override
	public void parseRequestParameters(Map<String, String> params, Map<String, FileItem> files) throws RequestParseException {
          try {
                boolean multiPart = isMultiPart();
                if( multiPart ) {
                    LightWebUploadRequestContext ctx = new LightWebUploadRequestContext();
                    FileUpload upload = new FileUpload();
                    List items = upload.parseRequest( (RequestContext) ctx );

                    parseQueryString( params );

                    for( Object o : items ) {
                        org.apache.commons.fileupload.FileItem item = (org.apache.commons.fileupload.FileItem) o;
                        if( item.isFormField() ) {
                            params.put( item.getFieldName(), item.getString() );
                        } else {
                            files.put( item.getFieldName(), new FileItemWrapper( item ) );
                        }
                    }
                } else {
                    InputStream in = request.body;
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    StreamUtils.readTo( in, out );
                    String rawBody = out.toString();
                    // don't trust xlightweb's param parsing because it doesnt preserve order
                    if( rawBody.length() > 0 ) {
                        parseParamters( rawBody, "utf-8", params);
                    }
                    String query = request.querystring;
                    if( query != null && query.length() > 0 ) {
                        parseParamters( query, "utf-8", params);
                    }
                }
            } catch( FileUploadException ex ) {
                throw new RequestParseException( "FileUploadException", ex );
            } catch( Throwable ex ) {
                throw new RequestParseException( ex.getMessage(), ex );
            }
	}

  private void parseQueryString( Map<String, String> map ) {
        String qs = request.querystring;
        parseQueryString( map, qs );
    }

    private void parseQueryString( Map<String, String> map, String qs ) {
        if( qs == null ) return;
        String[] nvs = qs.split( "&" );
        for( String nv : nvs ) {
            String[] parts = nv.split( "=" );
            String key = parts[0];
            String val = null;
            if( parts.length > 1 ) val = parts[1];
            if( val != null ) {
                try {
                    val = URLDecoder.decode( val, "UTF-8" );
                } catch( UnsupportedEncodingException ex ) {
                    throw new RuntimeException( ex );
                }
            }
            map.put( key, val );
        }
    }

	 private void parseParamters( String txt, String encoding, Map<String, String> map ) {
            try {
                String[] params = txt.split( "&" );
                for( String param : params ) {
                    String[] kv = param.split( "=" );
                    if( kv.length > 1 ) {
                        String name = URLDecoder.decode( kv[0], encoding );
                        String value = URLDecoder.decode( kv[1], encoding );
                        System.out.println( "form field: " + name);
                        map.put(name, value);
                    }
                }
            } catch( UnsupportedEncodingException use ) {
                throw new RuntimeException( use.toString() );
            }
        }

	private boolean isMultiPart() {
        String ct = request.contentType;
        return ct != null && ct.contains( Response.MULTIPART );
    }

	@Override
	public Cookie getCookie(String s) {
		 throw new UnsupportedOperationException( "Not supported yet." );
	}

	@Override
	public List<Cookie> getCookies() {
		 throw new UnsupportedOperationException( "Not supported yet." );
	}

	public class LightWebUploadRequestContext implements RequestContext {

        public String getCharacterEncoding() {
            return "utf-8";
        }

        public String getContentType() {
            return request.contentType;
        }

        public int getContentLength() {
			try {
				return request.body.available();
			} catch (IOException e) {
				e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
			}
			return 0;
		}

        public InputStream getInputStream() throws IOException {
            return request.body;
        }
    }
}
