package milton;

import com.bradmcevoy.http.AbstractResponse;
import com.bradmcevoy.http.Cookie;
import com.bradmcevoy.http.Response;
import play.mvc.Http;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Piero Sartini
 */

public class MiltonResponse extends AbstractResponse {

        private Http.Response response;
        private Status status;

	public MiltonResponse(Http.Response response) {
		 this.response = response;
	}
	
        public Status getStatus() {
            return status;
        }

        public Map<String, String> getHeaders() {
            Map<String, String> headers = new HashMap<String, String>();
            for( Map.Entry<String,Http.Header> entry : response.headers.entrySet()) {
                headers.put( entry.getKey(), entry.getValue().value() );
            }
            return headers;
        }

        public void setStatus( Status status ) {
            response.status = status.code;
        }

        public void setNonStandardHeader( String name, String value ) {
            response.headers.put(name, new Http.Header(name, value));
        }

        public String getNonStandardHeader( String name ) {
            if( response != null ) {
                return response.headers.get( name ).value();
            } else {
                return "";
            }
        }

        public OutputStream getOutputStream() {
            return response.out;
        }

        @Override
        public void close() {
			System.out.println("close");
        }

        public void setAuthenticateHeader( List<String> challenges ) {
            for( String ch : challenges ) {
                response.headers.put( Response.Header.WWW_AUTHENTICATE.code, new Http.Header(Response.Header.WWW_AUTHENTICATE.code, ch));
            }
        }

        public Cookie setCookie( Cookie cookie ) {
            throw new UnsupportedOperationException( "Not supported yet." );
        }

        public Cookie setCookie( String name, String value ) {
            throw new UnsupportedOperationException( "Not supported yet." );
        }
}