package controllers;

import milton.HttpManagerFactory;
import milton.MiltonRequest;
import milton.MiltonResponse;
import play.mvc.Controller;

/**
 * @author Piero Sartini
 */
public class Module extends Controller {

	public static void index() {
		MiltonRequest mReq = new MiltonRequest(request);
		MiltonResponse mRes = new MiltonResponse(response);
		HttpManagerFactory.getHttpManager().process(mReq, mRes);
	}
}
