/*
 * Copyright © Sartini IT Solutions, 2010.
 */

package models;

import java.io.Serializable;
import java.util.Date;

/**
 * Entity interface. see AbstractEntityBean as well.
 * 
 * @author Pierangelo Sartini
 */
public interface EntityBean extends java.io.Serializable {
    Serializable getId();

    Date getLastChanged();

    Date getCreated();
}
