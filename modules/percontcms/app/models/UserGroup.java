/*
 * Copyright © Sartini IT Solutions, 2010.
 */

package models;


import javax.persistence.Entity;

/**
 * @author Pierangelo Sartini
 */
@Entity
public class UserGroup extends AbstractEntityBean {
	private String title;
	private String description;

	public UserGroup() {
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
}
