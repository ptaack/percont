/*
 * Copyright © Sartini IT Solutions, 2010.
 */

package models;


import exceptions.NoLayoutException;

import javax.persistence.Entity;
import javax.persistence.Lob;

import play.data.validation.MaxSize;
import play.data.validation.Required;

/**
 * @author Pierangelo Sartini
 */
@Entity
public class Layout extends AbstractEntityBean {
	public String title;
	
    @Lob
    @Required
    @MaxSize(10000)
	public String content;
	public String contentType;

	public Layout() {
	}

	public Layout(String title) {
		this.title = title;
	}

	/**
	 * Loads the Layout for the given page, inheriting from its parents.
	 * 
	 * @param page
	 * @throws NoLayoutException() if no layout was assigned to the page or any of its parents.
	 * @return inherited layout
	 *
	 */
	public static Layout getForPage(Page page) {
		if (page.layout == null) {
			if (page.isRoot()) {
				throw new NoLayoutException();
			}
			return getForPage(page.parent);
		}
		return page.layout;
	}
}