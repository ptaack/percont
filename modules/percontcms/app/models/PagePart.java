/*
 * Copyright © Sartini IT Solutions, 2010.
 */

package models;


import exceptions.PartNotFoundException;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

import play.data.validation.MaxSize;
import play.data.validation.Required;

/**
 * @author Pierangelo Sartini
 */
@Entity
public class PagePart extends AbstractEntityBean {
	public String title;
	
    @Lob
    @Required
    @MaxSize(10000)
    public String content;
	@ManyToOne public Page page;

	public PagePart() {
		
	}

	/**
	 * Finds a PagePart to the given page and title. Using inheritance.
	 * 
	 * @param page
	 * @param title
	 * @return
	 */
	public static PagePart getByPageAndTitle(Page page, String title) {
		PagePart part = find("byPageAndTitle", page, title).first();
		if (part == null) {
			if (page.isRoot()) {
				throw new PartNotFoundException();
			}
			return getByPageAndTitle(page.parent, title);
		}
		return part;
	}
}
