package models;

import javax.persistence.Entity;
import javax.persistence.Lob;

import controllers.CRUD;

import play.data.validation.MaxSize;
import play.data.validation.Required;
import play.db.jpa.Model;

@Entity
public class Snippet extends AbstractEntityBean {
	@Required
	public String title;
	
    @Lob
    @MaxSize(10000)
    public String content;
}
