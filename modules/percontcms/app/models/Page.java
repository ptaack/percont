/*
 * Copyright © Sartini IT Solutions, 2010.
 */
package models;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Pierangelo Sartini
 */
@Entity
public class Page extends AbstractEntityBean {

	public String title;
	@ManyToOne
	public Page parent;
	public String path;
	public String domain;

	@OneToMany(mappedBy = "parent", cascade = CascadeType.ALL)
	@OrderBy("sorting, created DESC")
	public List<Page> childs;

	@OneToMany(mappedBy = "page", cascade = CascadeType.ALL)
	public List<PagePart> parts;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = true, cascade = CascadeType.REFRESH)
	public Layout layout;

	public int sorting;
	
	public Page() {
		if (this.parts == null) {
			parts = new ArrayList<PagePart>();
		}
	}
	
	public String toString() {
		return title;
	}

	/**
	 * Checks if this page instance is at the page tree's root.
	 *
	 * @return if page is a root page
	 */
	public boolean isRoot() {
		return parent == null;
	}

	/**
	 * Checks if this page has any children.
	 * Used within the pagetree.html tag.
	 *
	 * @return if page has children
	 */
	public boolean hasChildren() {
		return childs.size() > 0;
	}

	/**
	 * Checks if this page or any of its parents is configured for
	 * the given domain.
	 *
	 * @param domain
	 * @return
	 */
	public boolean hasInheritedDomain(String domain) {
		return hasInheritedDomain(this, domain);
	}

	/**
	 * Checks if the given page or any of its parents is configured for
	 * the given domain.
	 *
	 * @param page
	 * @param domain
	 * @return
	 */
	private boolean hasInheritedDomain(Page page, String domain) {
		if (page.domain == null || page.domain.equals("")) {
			if (page.isRoot()) {
				return false;
			}
			return hasInheritedDomain(page.parent, domain);
		}
		return page.domain.trim().equalsIgnoreCase(domain) || page.domain.trim().equals("*");
	}

	/**
	 * Returns the first layout found going up the page tree.
	 *
	 * @return Layout of the page
	 */
	public Layout getInheritedLayout() {
		return Layout.getForPage(this);
	}

	/**
	 * Returns a PagePart with the given name. If not found in this instance,
	 * it tries to inherit from parent pages.
	 *
	 * @param title
	 * @return
	 */
	public PagePart getInheritedPart(String title) {
		return PagePart.getByPageAndTitle(this, title);
	}

	/**
	 * Returns all childs of the given parent page.
	 *
	 * @param parentId the id of the parent page
	 * @return list of Page instances
	 */
	public static List<Page> findByParent(Long parentId) {
		if (parentId == null) {
			return find("select p from Page p where p.parent is null").fetch();
		}
		return find("byParent", parentId).fetch();
	}

	/**
	 * Finds all childs to the given parent and the slug.
	 *
	 * @param parent the parent page searched under
	 * @param slug   part of the URL
	 * @return a page if one is found or null
	 */
	public static Page findChild(Page parent, String slug) {
		List<Page> pages = Page.find("byPathAndParent", slug, parent).fetch();
		if (pages.size() > 0) {
			return pages.get(0);
		} else {
			return null;
		}
	}

	/**
	 * Finds a root page for a given domain.
	 *
	 * @param domain
	 * @return
	 */
	public static Page findRootPageByDomain(String domain) {
		// find root page for current domain
		List<Page> pages = Page.find("byPathAndDomain", "/", domain).fetch();
		// if no result, try with wildcard domain (*)
		if (pages.size() == 0) {
			pages = Page.find("byPathAndDomain", "/", "*").fetch();
		}
		if (pages.size() > 0) {
			return pages.get(0);
		} else {
			return null;
		}
	}

	/**
	 * Finds a page with the given path and domain. Using inheritance for domain.
	 *
	 * @param path
	 * @param domain
	 * @return
	 */
	public static Page findByPathAndInheritedDomain(String path, String domain) {
		// maybe an absolute path is given
		List<Page> pages = Page.find("byPath", path).fetch();
		Page page = null;
		for (Page p : pages) {
			if (p.hasInheritedDomain(domain)) {
				page = p;
				break;
			}
		}
		return page;
	}

	public static Page findPageForRequest(String domain, String path) {
		// strip trailing slash if not "/"
		if (path.length() > 1 && path.endsWith("/")) {
			path = path.substring(0, path.length() - 1);
		}
		// first, try to find page by absolute path and inherited domain
		Page page = Page.findByPathAndInheritedDomain(path, domain);
		// if no path found, go down the slugs from root
		if (page == null) {
			page = Page.findRootPageByDomain(domain);
			String mypath = path.substring(1).trim();
			for (String pp : mypath.split("/")) {
				page = Page.findChild(page, pp);
				if (page == null) {
					break;
				}
			}
		}
		return page;
	}
}
