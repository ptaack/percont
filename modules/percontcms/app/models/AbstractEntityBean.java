/*
 * Copyright © Sartini IT Solutions, 2010.
 */
package models;

import play.db.jpa.Model;

import javax.persistence.*;
import java.util.Date;

/**
 * Provides functionality available to every Entity in the system.
 * Mainly used to set lastChanged and created dates.
 * 
 * @author Pierangelo Sartini
 */
@MappedSuperclass
public abstract class AbstractEntityBean extends Model implements java.io.Serializable, EntityBean {

	@Temporal(TemporalType.TIMESTAMP)
	public Date lastChanged;
	@Temporal(TemporalType.TIMESTAMP)
	public Date created;

	@Override
	public String toString() {
		return this.getClass().getSimpleName() + "[id=" + getId() + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getLastChanged() {
		return lastChanged;
	}

	public Date getCreated() {
		return created;
	}

	@PostPersist
	public void postPersist() {
		created = new Date();
	}

	@PostUpdate
	public void postUpdate() {
		lastChanged = new Date();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof AbstractEntityBean)) {
			return false;
		}

		AbstractEntityBean that = (AbstractEntityBean) o;

		return id.equals(that.id);

	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}
}
