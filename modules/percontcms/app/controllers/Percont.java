package controllers;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import models.GalleryFile;
import models.Page;
import play.mvc.Controller;
import play.templates.Template;
import play.templates.TemplateLoader;

public class Percont extends Controller {

	public static void frontend() {
		renderArgs.data.remove("out");
		Page page = Page.findPageForRequest(request.domain, request.path);
		
		if (page != null && page.parts.size() >= 1) {
			Template templatePlay = TemplateLoader.loadString(page.getInheritedLayout().content);			
			Map args = new HashMap();
			args.put("page", page);
			//renderTemplate(templatePlay.name, page);
			renderHtml(templatePlay.render(args));
		} else {
			notFound();
		}
	}
}