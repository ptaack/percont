/*
 * Copyright © Sartini IT Solutions, 2010
 */
package controllers.admin;

import models.EntityBean;
import models.GalleryFile;
import models.Layout;
import models.Page;
import models.PagePart;
import play.mvc.Controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Pierangelo Sartini
 */
public class Pages extends Controller {

	public static void list() {
		List<Page> pages = Page.findByParent(null);
		render(pages);
	}

	public static void createChild(Long parentId) {
		List<Layout> layouts = Layout.findAll();
		render("admin/Pages/form.html", parentId, layouts);
	}

	public static void delete(Long pageId) {
		Page p = Page.findById(pageId);
		p.delete();
		list();
	}

	public static void edit(Long pageId) {
		Page page = Page.findById(pageId);
		List<Layout> layouts = Layout.findAll();
		render("admin/Pages/form.html", page, layouts);
	}

	public static void save(Page page, List<PagePart> parts) {
		Map<Serializable, PagePart> partMap = createMap(parts);
		if (page.id != null) {
			Page dbPage = Page.findById(page.id);
			List<PagePart> toDelete = new ArrayList<PagePart>();
			// edit and mark for deletion
			for (PagePart part : dbPage.parts) {
				Long id = part.id;
				if (partMap.containsKey(id)) {
					PagePart p = partMap.get(id);
					part.title = p.title;
					part.content = p.content;
					part.save();
					partMap.remove(id); // remove from map - only new parts remain
				} else {
					toDelete.add(part);
				}
			}
			// add new parts
			for (PagePart p : partMap.values()) {
				p.page = dbPage;
				page.parts.add(p.<PagePart>save());
			}
			// delete marked parts
			for (PagePart part : toDelete) {
				page.parts.remove(part);
				part.delete();
			}
			page.save();
		} else {
			Long parentId = params.get("parentId", Long.class);
			page.parent = Page.findById(parentId);
			page.save();
			// add new parts
			for (PagePart p : partMap.values()) {
				p.page = page;
				p.save();
			}
		}

		// finally save the page

		list();
	}

	/**
	 * Helper method which transforms a List of entities into a
	 * HashMap, using id as key.
	 *
	 * @param entityList
	 * @param <T>
	 * @return
	 */
	private static <T extends EntityBean> Map<Serializable, T> createMap(List<T> entityList) {
		Map<Serializable, T> entityMap = new HashMap<Serializable, T>();
		Long z = 100L;
        if (entityList != null) {
            for (T e : entityList) {
                if (e != null) {
                    if (e.getId() == null) {
                        entityMap.put(z++, e);
                    } else {
                        entityMap.put(e.getId(), e);
                    }
                }
            }
        }
		return entityMap;
	}
}
