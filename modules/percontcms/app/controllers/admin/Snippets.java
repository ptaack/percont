package controllers.admin;

import java.util.List;

import models.Layout;
import models.Page;
import models.Snippet;
import play.db.jpa.Model;
import play.mvc.Controller;

public class Snippets extends Controller {
	public static void list() {
		List<Page> snippets = Snippet.findAll();
		render(snippets);
	}

	public static void create() {
		render("admin/Snippets/form.html");
	}

	public static void edit(Long id) {
		Snippet snippet = Snippet.findById(id);
		render("admin/Snippets/form.html", snippet);
	}

	public static void delete(Long id) {
		Snippet snippet = Snippet.findById(id);
		snippet.delete();
		list();
	}

	public static void save(Snippet snippet) {
		snippet.save();
		list();
	}
}
