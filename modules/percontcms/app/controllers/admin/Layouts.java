/*
 * Copyright © Sartini IT Solutions, 2010.
 */

package controllers.admin;

import models.Layout;
import models.Page;
import play.mvc.Controller;

import java.util.List;

/**
 * @author Pierangelo Sartini
 */
public class Layouts extends Controller {
	public static void list() {
		List<Page> layouts = Layout.findAll();
		render(layouts);
	}

	public static void create() {
		render("admin/Layouts/form.html");
	}

	public static void edit(Long id) {
		Layout layout = Layout.findById(id);
		render("admin/Layouts/form.html", layout);
	}

	public static void delete(Long id) {
		
	}

	public static void save(Layout layout) {
		layout.save();
		
        if (params.get("_save") != null) {
            redirect(request.controller + ".list");
        }
        if (params.get("_saveAndContinue") != null) {
            redirect(request.controller + ".edit", layout.id);
        }

	}
}
