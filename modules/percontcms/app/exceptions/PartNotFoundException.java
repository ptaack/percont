/*
 * Copyright © Sartini IT Solutions, 2010.
 */

package exceptions;

/**
 * @author Pierangelo Sartini
 */
public class PartNotFoundException extends RuntimeException {
}
