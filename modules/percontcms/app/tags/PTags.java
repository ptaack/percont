/*
 * Copyright © Sartini IT Solutions, 2010.
 */
package tags;

import groovy.lang.Closure;
import models.Page;
import models.PagePart;
import models.Snippet;
import play.exceptions.TemplateNotFoundException;
import play.templates.FastTags;
import play.templates.GroovyTemplate;
import play.templates.Template;
import play.templates.TemplateLoader;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import play.templates.TagContext;
import static controllers.Const.PAGE_CONTEXT;

/**
 * @author Pierangelo Sartini
 */
@FastTags.Namespace("")
public class PTags extends FastTags {

	public static void _content(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
		try {
			String part = "body";
			boolean inherit, contextual;
			if (args.get("part") != null) {
				part = args.get("part").toString();
			}
			if (args.get("arg") != null) {
				part = args.get("arg").toString();
			}
			if (args.get("inherit") != null) {
				inherit = args.get("inherit").toString().equalsIgnoreCase("true");
			}
			if (args.get("contextual") != null) {
				contextual = args.get("contextual").toString().equalsIgnoreCase("true");
			}

			Page ctxPage = (Page) template.getBinding().getVariable(PAGE_CONTEXT);
			PagePart renderPart = ctxPage.getInheritedPart(part);

			Template t = TemplateLoader.load("partTemplate", renderPart.content);
			Map<String, Object> newArgs = new HashMap<String, Object>();
			newArgs.putAll(template.getBinding().getVariables());
			newArgs.put("_isInclude", true);
			t.render(newArgs);
		} catch (TemplateNotFoundException e) {
			throw new RuntimeException("Template not found!");
		}
	}

	public static void _children(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
		try {
			int offset, limit;
			String by, status;
			Page ctxPage = (Page) template.getBinding().getVariable("page");
			for (Page child : ctxPage.childs) {
				template.getBinding().setVariable(PAGE_CONTEXT, child);
				body.call();
			}
			template.getBinding().setVariable(PAGE_CONTEXT, ctxPage);
		} catch (TemplateNotFoundException e) {
			throw new RuntimeException("Template not found!");
		}
	}

	public static void _title(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
		try {
			Page ctxPage = (Page) template.getBinding().getVariable(PAGE_CONTEXT);
			out.print(ctxPage.title);
		} catch (TemplateNotFoundException e) {
			throw new RuntimeException("Template not found!");
		}
	}
	
	public static void _snippet(Map<?, ?> args, Closure body, PrintWriter out, GroovyTemplate.ExecutableTemplate template, int fromLine) {
		final String _arg = args.get("arg").toString();
		Snippet s = Snippet.find("byTitle", _arg).first();
		if (s != null) {
			out.write(s.content);
		}
	}
}