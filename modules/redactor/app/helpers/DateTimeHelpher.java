package helpers;

import java.text.SimpleDateFormat;
import java.util.Date;

import sun.java2d.pipe.SpanShapeRenderer.Simple;

public class DateTimeHelpher {
	public static String getCurrentDate() {
		Date d = new Date();
		SimpleDateFormat format = new SimpleDateFormat ("yyyyMMdd");
		return format.format(d);
	}
}
