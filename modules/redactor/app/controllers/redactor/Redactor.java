package controllers.redactor;


import helpers.DateTimeHelpher;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.time.DateUtils;

import play.*;
import play.db.jpa.Blob;
import play.mvc.*;


import models.*;

public class Redactor extends Controller {
	public static String uploadBasePath = Play.configuration.getProperty("percont.upload_path", "public/uploads/percont/");
	public static String uploadFilePath = uploadBasePath + DateTimeHelpher.getCurrentDate().concat("/");
	
	@Before
	public static void initPath() {
		File _uploadFilePath = new File(uploadFilePath);
		if (!_uploadFilePath.exists()) {
			_uploadFilePath.mkdirs();;
		}
		new File(uploadFilePath).mkdirs();
	}
	
	private static File upload(File file) throws IOException  {
//    	String uploadPath = Play.configuration.getProperty("percont.image_path", "public/uploads/percont/");
//    	String fileName =  file.getName();
    	String fileExt = "." + FilenameUtils.getExtension(file.getName());
    	
    	/*move block to external method*/
    	MessageDigest messageDigest = null;
		try {
			messageDigest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
    	messageDigest.reset();
    	
    	Date d = new Date();
    	
    	messageDigest.update(d.toString().getBytes(Charset.forName("UTF8")));

    	final byte[] resultByte = messageDigest.digest();
    	final String resultName = new String(Hex.encodeHex(resultByte));
    	/*end block*/
    	
//    	File resultFile = new File(uploadFilePath, fileName.concat("_").concat(resultName).concat(fileExt));
    	File resultFile = new File(uploadFilePath, resultName.concat(fileExt));
    	FileUtils.copyFile(file, resultFile);
    	
    	FileWriter wr = new FileWriter(new File(uploadFilePath, resultName.concat(".name")));
    	wr.write(file.getName());
    	wr.close();
    	
    	return resultFile;
	}
	
	private static String getIco(String type) {
		Map<String, String> fileicons =  new HashMap<String, String>();
		fileicons.put("avi", "avi");
		fileicons.put("doc", "doc");
		fileicons.put("docx", "doc");
		fileicons.put("gif", "gif");
		fileicons.put("jpg", "jpg");
		fileicons.put("jpeg", "jpg");
		fileicons.put("mov", "mov");
		fileicons.put("csv", "csv");
		fileicons.put("html", "html");
		fileicons.put("pdf", "pdf");
		fileicons.put("png", "png");
		fileicons.put("ppt", "ppt");
		fileicons.put("rar", "rar");
		fileicons.put("rtf", "rtf");
		fileicons.put("txt", "txt");
		fileicons.put("xls", "xls");
		fileicons.put("xlsx", "xls");
		fileicons.put("zip", "zip");

		if (fileicons.containsKey(type)) {
			return fileicons.get(type);
		} else {
			return "other";
		}
	}
	
	private static File getUploadFile(String folder, String file) {
		return new File(new File(uploadBasePath, folder), file);
	}
	
	private static File getUploadFileName(File file) {
		return new File(file.getParentFile(), FilenameUtils.getBaseName(file.getName()).concat(".name"));
	}
	
	private static String readUploadFileName(File file) throws IOException {
    	String realName = "";
		FileReader reader = new FileReader(getUploadFileName(file));
		BufferedReader buff = new BufferedReader(reader);
		realName = buff.readLine();
		buff.close();
		reader.close();
		return realName;
	}
	
	/**
	 * Return static URL to uploaded file
	 * @param file
	 * @throws IOException
	 */
    public static void imageUpload(File file) throws IOException {
    	File uploadFile = upload(file);
    	renderHtml("/".concat(uploadFilePath).concat(uploadFile.getName()));
    }
    
    public static void fileUpload(File file) throws IOException {
    	File uploadFile = upload(file);
    	renderHtml(String.format("<a href=\"javascript:void(null);\" rel=\"%1$s\" class=\"redactor_file_link redactor_file_ico_%2$s\">%3$s</a>;", 
    			uploadFile.getParentFile().getName() + "/" + uploadFile.getName(),
    			getIco(FilenameUtils.getExtension(uploadFile.getName())),
    			file.getName()));
    }
    
    public static void fileDownload(String folder, String file) throws IOException {
    	File _file = getUploadFile(folder, file);
    	if (!_file.exists()) {
    		notFound();
    	}
    	
    	FileNameMap fileNameMap = URLConnection.getFileNameMap();
    	String mimeType= fileNameMap.getContentTypeFor(file);
//    	String mimeType= URLConnection.guessContentTypeFromName(file);
    	
    	response.setContentTypeIfNotSet(mimeType);
    	renderBinary(_file, readUploadFileName(_file));
    }
    
    public static void fileDelete(String folder, String file) throws IOException {
    	File _file = getUploadFile(folder, file);
    	File _fileName = getUploadFileName(_file);
    	_file.delete();
    	_fileName.delete();
    }
}